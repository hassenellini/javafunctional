package imperative;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static imperative.Gender.FEMALE;
import static imperative.Gender.MALE;

public class Main {

    public static void main(String[] args) {

        List<Person> people = List.of(
        new Person("John", MALE),
        new Person("Mariah", FEMALE),
        new Person("Aisha", FEMALE),
        new Person("Alex", MALE),
        new Person("Alice", FEMALE)
        );


        // Imperative approach
        System.out.println("---- Imperative approach ----");
        List<Person> females = new ArrayList<>();

        for (Person person: people){
            if (FEMALE.equals(person.getGender())){
                females.add(person);
            }
        }
        for (Person female: females){
            System.out.println(female);
        }

        // Declarative approach
        System.out.println("---- Declarative approach ----");
        // Predicate takes 1 argument and produces 1  boolean result
        List<Person> females2 = people.stream()
                .filter(person -> FEMALE.equals(person.getGender()))
                .collect(Collectors.toList());

        females2.forEach(System.out::println);

    }


}

