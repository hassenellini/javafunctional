package optionals;

import java.util.Optional;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        Supplier<IllegalStateException> exception = () -> new IllegalStateException("exception");
        Optional.ofNullable("hassen@gmail.com")
//                .orElseGet(() -> "default value");
//                  .orElseThrow(exception);
        .ifPresentOrElse(email -> System.out.println("sending email to "+email),
                ()-> System.out.println("cannot send email")

                );

//        System.out.println(value);
    }
}
