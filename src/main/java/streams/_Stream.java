package streams;

import imperative.Gender;
import imperative.Person;

import javax.imageio.plugins.jpeg.JPEGImageReadParam;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static imperative.Gender.FEMALE;
import static imperative.Gender.MALE;
import static imperative.Gender.OTHER;

public class _Stream {
    public static void main(String[] args) {

        List<Person> people = List.of(
                new Person("John", MALE),
                new Person("Mariah", FEMALE),
                new Person("Aisha", FEMALE),
                new Person("Alex", MALE),
                new Person("Alice", FEMALE),
                new Person("Med", OTHER),
                new Person("Saida", OTHER)
        );

//        people.stream()
//                .map(person -> person.getName())
//                .mapToInt(String::length)
//                .forEach(System.out::println);

        Predicate<Person> personPredicate = person -> person.getGender().equals(OTHER);
        boolean containsOnlyFemales = people.stream()
                .noneMatch(personPredicate);
        System.out.println(containsOnlyFemales);


    }
}
