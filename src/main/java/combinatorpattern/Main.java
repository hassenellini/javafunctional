package combinatorpattern;

import java.time.LocalDate;
import java.util.Optional;

import static combinatorpattern.CustomerRegistrationValidator.*;

public class Main {

    public static void main(String[] args) {
        Customer customer = new Customer(
                "Hassen",
                "hassen@gmail.com",
                "0021658747775",
                LocalDate.of(1998, 04, 01)
        );

//        CustomerValidatorService validatorService = new CustomerValidatorService();
//
//        System.out.println(validatorService.isValid(customer));


        // using Combinator pattern

        ValidationResult result = isEmailValid()
                .and(isPhoneNumberValid())
                .and(isAdult())
                .apply(customer);

        System.out.println(result);

        Optional.ofNullable(!result.equals(ValidationResult.SUCCESS))
                .orElseThrow(() -> new IllegalStateException(result.name()));
    }
}
