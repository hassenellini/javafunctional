package functionalinterface;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class _Predicate {

    public static void main(String[] args) {

        System.out.println("----Imperative----");
        System.out.println(isPhoneNumberValid("04020202"));
        System.out.println(isPhoneNumberValid("03020202"));
        System.out.println(isPhoneNumberValid("040020202"));
        System.out.println("----Declaritive----");

        // Predicate takes 1 argument and produces 1  boolean result
        System.out.println(isPhoneNumberValidPredicate.test("04020202"));
        System.out.println(isPhoneNumberValidPredicate.test("03020202"));
        System.out.println(isPhoneNumberValidPredicate.test("040020202"));

        System.out.println(
                "Is phone number valid and contains number 3 "+
                isPhoneNumberValidPredicate.and(contains3).test("04020203")
        );
        System.out.println(
                "Is phone number valid or contains number 3 "+
                        isPhoneNumberValidPredicate.or(contains3).test("040202003")
        );


    }


    static Predicate<String> isPhoneNumberValidPredicate = phoneNumber -> phoneNumber.startsWith("04") && phoneNumber.length() == 8;
    static Predicate<String> contains3 = phoneNumber -> phoneNumber.contains("3");


    static boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("04") && phoneNumber.length() == 8;
    }
}
