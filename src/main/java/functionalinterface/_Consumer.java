package functionalinterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {

    public static void main(String[] args) {
        // Normal java function
        System.out.println("----Consumer------");
        Customer hassen = new Customer("Hassen", "58747775");
        greetCustomer(hassen);


        // Consumer functional Interface
        // Consumer takes 1 argument and return 0 result = void
        greetCustomerConsumer.accept(hassen);

        // BiConsumer
        // Consumer takes 2 argument and return 0 result = void
        // argument have to be between ()
        System.out.println("----BiConsumer------");
        greetCustomerBiConsumer.accept(hassen,true);
        greetCustomerBiConsumer.accept(hassen,false);

    }


    static Consumer<Customer> greetCustomerConsumer = customer ->
            System.out.println("Hello " + customer.customerName +
                    ",thanks for registering phone number " +
                    customer.customerPhoneNumber);

    static BiConsumer<Customer,Boolean> greetCustomerBiConsumer = (customer, showPhoneNumber) ->
            System.out.println("Hello " + customer.customerName +
                    ",thanks for registering phone number " +
                    (showPhoneNumber ? customer.customerPhoneNumber : "********")

            );

    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.customerName +
                ",thanks for registering phone number " +
                customer.customerPhoneNumber);
    }


    static class Customer {
        private final String customerName;
        private final String customerPhoneNumber;

        public Customer(String customerName, String getCustomerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = getCustomerPhoneNumber;
        }
    }
}
